var express = require("express");
var app = express();
var port = 3700;
 
var Globalvars = {
	"users" : []
}


app.get("/users", function(req, res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.send(JSON.stringify(Globalvars.users));
});

 
var io = require('socket.io').listen(app.listen(port));
/*io.set('transports', [
    'websocket'
]);*/

console.log("Listening on port " + port);


io.sockets.on('connection', function (socket) {
	socket.on('login', function (userdata) {
		//console.log(userdata.first_name);
		if(ifUserAlreadyExists(userdata.id)){
			console.log(userdata.first_name + " " + userdata.last_name + " already exists");
		}
		else{
			var user = new Object;
			user.id = userdata.id;
			user.username = userdata.username;
			user.first_name = userdata.first_name;
			user.last_name = userdata.last_name;
			user.image = "https://graph.facebook.com/" + userdata.id + "/picture?width=100&height=100";
			user.status = "available";
			user.socketId = socket.id;

			Globalvars.users.push(user);
		}

		console.log("user joined");
		console.log(Globalvars.users);
		io.sockets.emit('userchannel', Globalvars.users);
	});

	socket.on('call', function (data) {
		console.log("called");
		io.sockets.socket(getUserFromId(data.receiver_id).socketId).emit("call", data);
	});

	socket.on('disconnect', function () {
		removeUserUsingSocketId(socket.id);
		console.log("user disconnected");
		console.log(Globalvars.users);
		io.sockets.emit('userchannel', Globalvars.users);
	});
});


function ifUserAlreadyExists(id){
	for(i = 0; i < Globalvars.users.length; i++){
		if(Globalvars.users[i].id == id){
			return true;
		}
	}

	return false;
}

function getUserUsingSocketId(socketId){
	for(i = 0; i < Globalvars.users.length; i++){
		if(Globalvars.users[i].socketId == socketId){
			return Globalvars.users[i];
		}
	}
}

function removeUserUsingSocketId(socketId){
	for(i = 0; i < Globalvars.users.length; i++){
		if(Globalvars.users[i].socketId == socketId){
			Globalvars.users.splice(i, 1);
			break;
		}
	}
}

function getUserFromId(id){
	for(i = 0; i < Globalvars.users.length; i++){
		if(Globalvars.users[i].id == id){
			return Globalvars.users[i];
		}
	}
}